/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "channel_operations_mock.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {

ResultCode ChannelOperationsMock::MockChannelTransmit(SecureElementContext *context, const uint8_t *command,
    uint32_t commandLen, uint8_t *response, uint32_t *responseLen)
{
    if (auto *invoker = ChannelOperationsMock::GetInstance(); invoker) {
        return invoker->ChannelTransmit(context, command, commandLen, response, responseLen);
    }

    return SUCCESS;
}

ResultCode ChannelOperationsMock::MockChannelOpen(SecureElementContext *context)
{
    if (auto *invoker = ChannelOperationsMock::GetInstance(); invoker) {
        return invoker->ChannelOpen(context);
    }

    return SUCCESS;
}

ResultCode ChannelOperationsMock::MockChannelClose(SecureElementContext *context)
{
    if (auto *invoker = ChannelOperationsMock::GetInstance(); invoker) {
        return invoker->ChannelClose(context);
    }

    return SUCCESS;
}

ResultCode ChannelOperationsMock::MockChannelGetOpenResponse(SecureElementContext *context, uint8_t *response,
    uint32_t *responseLen)
{
    if (auto *invoker = ChannelOperationsMock::GetInstance(); invoker) {
        return invoker->ChannelGetOpenResponse(context, response, responseLen);
    }

    return SUCCESS;
}

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS
