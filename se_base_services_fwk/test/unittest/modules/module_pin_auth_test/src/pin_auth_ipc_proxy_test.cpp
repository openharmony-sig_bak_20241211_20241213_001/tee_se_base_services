/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "ipc_transmit_mock.h"
#include "pin_auth_ipc_proxy.h"
#include "test_helpers.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace testing;
TEST(PinAuthIpcProxyTest, SePinGetNumSlotsProxyNullInput)
{
    {
        auto ret = SePinGetNumSlotsProxy(nullptr, nullptr);
        EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
    }
    {
        IpcTransmitMock mock;
        EXPECT_CALL(mock, IpcTransmit).Times(Exactly(0));
        auto ret = SePinGetNumSlotsProxy(IpcTransmitMock::MockIpcTransmit, nullptr);
        EXPECT_EQ(ret, INVALID_PARA_NULL_PTR);
    }
}
} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS