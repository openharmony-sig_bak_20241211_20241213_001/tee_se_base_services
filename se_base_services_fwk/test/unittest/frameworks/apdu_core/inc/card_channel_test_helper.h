/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TEST_CARD_CHANNEL_TEST_HELPER_H
#define TEST_CARD_CHANNEL_TEST_HELPER_H

#include "channel_operations_mock.h"

#include <memory>

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace std;
class CardChannelTestHelper {
public:
    explicit CardChannelTestHelper(const ChannelOperations *oper = nullptr)
    {
        if (oper) {
            channel_.context = new (std::nothrow) SecureElementContext;
            channel_.oper = *oper;
        }
        if (channel_.context != nullptr) {
            channel_.context->handle = nullptr;
        }
    }
    ~CardChannelTestHelper()
    {
        if (channel_.context != nullptr) {
            delete channel_.context;
        }
    }

    CardChannel *Get()
    {
        return &channel_;
    }

private:
    CardChannel channel_ {};
};

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS

#endif // TEST_CARD_CHANNEL_TEST_HELPER_H
