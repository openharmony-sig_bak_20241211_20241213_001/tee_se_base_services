/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "apdu_utils.h"

#include <stddef.h>

void DataRevert(uint8_t *data, uint32_t length)
{
    if (data == NULL || length % 2 != 0 || length == 0) { // the len must to be divisible by 2
        return;
    }

    uint8_t *ptr = (uint8_t *)data;
    uint32_t end = length - 1;
    for (uint32_t i = 0; i < length / 2; ++i) { // the len must to be divisible by 2
        ptr[i] ^= ptr[end - i];
        ptr[end - i] ^= ptr[i];
        ptr[i] ^= ptr[end - i];
    }
}

static CurrentTimeMillisGetter *g_timeGetter = NULL;

__attribute__((weak)) uint64_t GetCurrentTimeMillis(void)
{
    if (g_timeGetter == NULL) {
        return 0;
    }
    return g_timeGetter();
}

void SetCurrentTimeMillisGetter(CurrentTimeMillisGetter *getter)
{
    g_timeGetter = getter;
}