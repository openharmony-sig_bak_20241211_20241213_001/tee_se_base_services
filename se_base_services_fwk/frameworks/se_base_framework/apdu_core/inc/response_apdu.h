/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CORE_INC_RESPONSE_APDU_H
#define CORE_INC_RESPONSE_APDU_H

#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SW1(statusWords) (((statusWords)&0xFF00) >> 8)
#define SW2(statusWords) ((statusWords)&0xFF)

typedef struct ResponseApdu ResponseApdu;

void DestroyResponseApdu(ResponseApdu *apdu);

bool GetStatusWords(const ResponseApdu *responseApdu, uint16_t *statusWords);

bool CheckStatusWords(const ResponseApdu *responseApdu, uint16_t statusWords);

const uint8_t *GetResponseDataBuffer(const ResponseApdu *responseApdu, uint32_t *length);

bool GetResponseUint8ValueAt(const ResponseApdu *responseApdu, uint32_t index, uint8_t *value);

bool GetResponseUint16ValueAt(const ResponseApdu *responseApdu, uint32_t index, uint16_t *value);

bool GetResponseUint32ValueAt(const ResponseApdu *responseApdu, uint32_t index, uint32_t *value);

bool GetResponseBufferValueAt(const ResponseApdu *responseApdu, uint32_t index, uint8_t *value, uint32_t size);

#ifdef __cplusplus
}
#endif

#endif // CORE_INC_RESPONSE_APDU_H