add_library(logger_obj OBJECT src/logger.c)

target_include_directories(logger_obj PUBLIC inc)

target_include_directories(logger_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})

target_compile_options(logger_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})

if(OPTION_SECUREC_INDEPENDENT)
    message(STATUS "logger_obj use independent secure c.")
    target_link_libraries(logger_obj PRIVATE securec_interface)
endif()
