/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_IPC_DEFINES_H
#define CODE_MODULES_INC_STORAGE_IPC_DEFINES_H

#include <stdint.h>

#include "se_base_services_defines.h"
#include "se_module_common_defines.h"
#include "se_module_sec_storage_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SEC_STORAGE_WITH_CMD(cmd) SERVICE_WITH_CMD_ID(SERVICE_ID_SEC_STORAGE, cmd)

enum {
    CMD_SS_SET_FACTORY_RESET_AUTH_KEY = SEC_STORAGE_WITH_CMD(1),
    CMD_SS_GET_FACTORY_RESET_AUTH_ALGO = SEC_STORAGE_WITH_CMD(2),
    CMD_SS_PREPARE_FACTORY_REST = SEC_STORAGE_WITH_CMD(3),
    CMD_SS_PROCESS_FACTORY_REST = SEC_STORAGE_WITH_CMD(4),
    CMD_SS_SET_TO_USER_MODE = SEC_STORAGE_WITH_CMD(5),
    CMD_SS_GET_SLOT_OPER_ALGORITHM = SEC_STORAGE_WITH_CMD(6),
    CMD_SS_GET_FACTORY_RESET_ALGORITHM = SEC_STORAGE_WITH_CMD(7),
    CMD_SS_GET_ALL_SLOT_SIZE = SEC_STORAGE_WITH_CMD(8),
    CMD_SS_SET_ALL_SLOT_SIZE = SEC_STORAGE_WITH_CMD(9),
    CMD_SS_ALLOCATE_SLOT = SEC_STORAGE_WITH_CMD(10),
    CMD_SS_FREE_SLOT = SEC_STORAGE_WITH_CMD(11),
    CMD_SS_READ_SLOT = SEC_STORAGE_WITH_CMD(12),
    CMD_SS_WRITE_SLOT = SEC_STORAGE_WITH_CMD(13),
    CMD_SS_GET_SLOT_STATUS = SEC_STORAGE_WITH_CMD(14),
    CMD_SS_IS_SERVICE_AVAILABLE = SEC_STORAGE_WITH_CMD(CMD_IS_SERVICE_AVAILABLE),
    CMD_SS_SET_SERVICE_CONFIG = SEC_STORAGE_WITH_CMD(CMD_SET_SERVICE_CONFIG),
};

#define MAX_SLOTS_NUM 80

#define MAX_SLOT_KEY_SIZE 80

#define SEC_STORAGE_FILE_NAME_SIZE_MAX 16
typedef struct StorageFileName {
    uint8_t handle[SEC_STORAGE_FILE_NAME_SIZE_MAX];
} StorageFileName;

typedef struct StorageDataArea {
    uint16_t offset;
    uint16_t length;
} StorageDataArea;

typedef struct StorageIndicator {
    const StorageFileName *name;
    const StorageAuthKey *key;
    const StorageDataArea *area;
} StorageIndicator;

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_IPC_DEFINES_H