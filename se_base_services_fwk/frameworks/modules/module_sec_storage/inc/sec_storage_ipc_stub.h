/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_IPC_STUB_H
#define CODE_MODULES_INC_STORAGE_IPC_STUB_H

#include <stdint.h>

#include "card_channel.h"
#include "module_common_ipc_stub.h"
#include "se_base_services_defines.h"

typedef ResultCode SecStorageKeyDeriveProc(uint32_t sender, uint8_t *key, uint32_t len);

typedef ResultCode SecStorageSlotIdGetter(uint32_t sender, const char *name, uint8_t *slotId);

typedef ResultCode SecStorageServiceBindingKeyGetter(uint8_t *key, uint32_t *keyLength, uint32_t *kvn);

typedef struct SecStorageContext {
    SeCommonContext base;
    SecStorageKeyDeriveProc *derive;
    SecStorageSlotIdGetter *slotIdGetter;
    SecStorageServiceBindingKeyGetter *bindingKeyGetter;
} SecStorageContext;

#ifdef __cplusplus
extern "C" {
#endif

ResultCode ProcessSecStorageCommandStub(SecStorageContext *context, uint32_t cmd, SharedDataBuffer *buffer);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_IPC_STUB_H