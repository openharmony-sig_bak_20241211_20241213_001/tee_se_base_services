/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_STORAGE_IPC_PROXY_H
#define CODE_MODULES_INC_STORAGE_IPC_PROXY_H

#include "se_module_sec_storage_defines.h"
#include "sec_storage_ipc_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

ResultCode SeSecStorageSetFactoryResetAuthenticationKeyProxy(IpcTransmit *transmit, FactoryResetLevel level,
    FactoryResetAuthAlgo algo, const StorageAuthKey *key);

ResultCode SeSecStorageGetFactoryResetAuthenticationAlgoProxy(IpcTransmit *transmit, FactoryResetLevel level,
    FactoryResetAuthAlgo *algo);

ResultCode SeSecStoragePrepareFactoryResetProxy(IpcTransmit *transmit, uint8_t *nonce, uint32_t *length);

ResultCode SeSecStorageProcessFactoryResetProxy(IpcTransmit *transmit, FactoryResetLevel level,
    const uint8_t *credential, uint32_t length);

ResultCode SeSecStorageSetToUserModeProxy(IpcTransmit *transmit, const StorageUserModeConf *config);

ResultCode SeSecStorageIsSlotOperateAlgorithmSupportedProxy(IpcTransmit *transmit, SlotOperAlgo algo,
    uint32_t *available);

ResultCode SeSecStorageIsFactoryResetAlgorithmSupportedProxy(IpcTransmit *transmit, FactoryResetAuthAlgo algo,
    uint32_t *available);

ResultCode SeSecStorageAllocateSlotProxy(IpcTransmit *transmit, const StorageFileName *name,
    const StorageSlotAttr *slotAttr, const StorageAuthKey *slotKey);

ResultCode SeSecStorageGetSlotStatusProxy(IpcTransmit *transmit, const StorageFileName *name,
    StorageSlotStatus *status);

ResultCode SeSecStorageWriteSlotProxy(IpcTransmit *transmit, const StorageFileName *name, const StorageAuthKey *key,
    const StorageDataArea *area, const StorageDataBuffer *data);

ResultCode SeSecStorageReadSlotProxy(IpcTransmit *transmit, const StorageFileName *name, const StorageAuthKey *key,
    const StorageDataArea *area, StorageDataBuffer *data);

ResultCode SeSecStorageFreeSlotProxy(IpcTransmit *transmit, const StorageFileName *name, const StorageAuthKey *key);

ResultCode SeSecStorageSetAllSlotsSizeProxy(IpcTransmit *transmit, const uint16_t *slotSizeArray, uint32_t arrayLength);

ResultCode SeSecStorageGetAllSlotsSizeProxy(IpcTransmit *transmit, uint16_t *slotSizeArray, uint32_t *arrayLength);

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_IPC_PROXY_H