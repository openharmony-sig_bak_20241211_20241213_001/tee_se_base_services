add_library(se_modules_common_ipc_proxy_obj OBJECT src/proxy/module_common_ipc_proxy.c)
target_include_directories(se_modules_common_ipc_proxy_obj PUBLIC inc)
target_include_directories(se_modules_common_ipc_proxy_obj PRIVATE src/inc)
target_include_directories(se_modules_common_ipc_proxy_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(se_modules_common_ipc_proxy_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
target_link_libraries(se_modules_common_ipc_proxy_obj PRIVATE logger_obj)
target_link_libraries(se_modules_common_ipc_proxy_obj PRIVATE se_base_services_defines)
if(OPTION_SECUREC_INDEPENDENT)
    message(STATUS "se_modules_common_ipc_proxy_obj use independent secure c.")
    target_link_libraries(se_modules_common_ipc_proxy_obj PRIVATE securec_interface)
endif()

add_library(
    se_modules_common_ipc_stub_obj OBJECT
    src/stub/module_common_ipc_stub.c
    src/stub/module_common_core.c
    src/stub/module_common_core_inner.c
)
target_include_directories(se_modules_common_ipc_stub_obj PUBLIC inc)
target_include_directories(se_modules_common_ipc_stub_obj PRIVATE src/inc)
target_include_directories(se_modules_common_ipc_stub_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_INC})
target_compile_options(se_modules_common_ipc_stub_obj PRIVATE ${SE_BASE_SERVICES_DEFAULT_CC})
target_link_libraries(se_modules_common_ipc_stub_obj PRIVATE logger_obj)
target_link_libraries(se_modules_common_ipc_stub_obj PRIVATE parcel_obj)
target_link_libraries(se_modules_common_ipc_stub_obj PRIVATE se_base_services_defines)
target_link_libraries(se_modules_common_ipc_stub_obj PRIVATE se_apdu_core_obj)

if(OPTION_SECUREC_INDEPENDENT)
    message(STATUS "se_modules_common_ipc_stub_obj use independent secure c.")
    target_link_libraries(se_modules_common_ipc_stub_obj PRIVATE securec_interface)
endif()

if(ENABLE_TESTING)
    target_include_directories(se_modules_common_ipc_stub_obj PUBLIC src/stub)
endif()
