/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "module_common_ipc_proxy.h"

#include <securec.h>
#include <stddef.h>

#include "logger.h"
#include "module_common_ipc_defines.h"

ResultCode SeCommonIsServiceAvailableProxy(IpcTransmit *transmit, ServiceId sid, SeServiceStatus *status)
{
    if (transmit == NULL || status == NULL) {
        LOG_ERROR("param error, null transmit or status");
        return INVALID_PARA_NULL_PTR;
    }

    uint8_t *reply = (uint8_t *)status;
    uint32_t replyLen = sizeof(SeServiceStatus);

    uint32_t cmd = SERVICE_WITH_CMD_ID(sid, CMD_IS_SERVICE_AVAILABLE);
    ResultCode ret = transmit(cmd, (const uint8_t *)&sid, sizeof(ServiceId), reply, &replyLen);
    LOG_INFO("ret is %u, sid is %u, available is %u, version is %u, vendor is %u", ret, sid, status->available,
        status->version, status->config.vendor);

    return ret;
}

ResultCode SeCommonSetServiceConfigurationProxy(IpcTransmit *transmit, ServiceId sid, uint32_t lock,
    const SeServiceConfig *config)
{
    if (transmit == NULL || config == NULL) {
        LOG_ERROR("param error, null transmit or status");
        return INVALID_PARA_NULL_PTR;
    }
    SeCommonSetServiceConfInput input;
    (void)memset_s(&input, sizeof(SeCommonSetServiceConfInput), 0, sizeof(SeCommonSetServiceConfInput));
    input.sid = sid;
    input.lock = lock;
    input.config = *config;

    uint32_t cmd = SERVICE_WITH_CMD_ID(sid, CMD_SET_SERVICE_CONFIG);
    ResultCode ret = transmit(cmd, (const uint8_t *)&input, sizeof(SeCommonSetServiceConfInput), NULL, NULL);

    LOG_INFO("ret is %u, sid is %u, lock is %u, vendor is %u", ret, sid, lock, config->vendor);
    return ret;
}

ResultCode SeCommonSetBindingKeyProxy(IpcTransmit *transmit, uint8_t *initKey, uint32_t initKeyLength, uint32_t initKvn,
    ServiceId sid)
{
    if (transmit == NULL || initKey == NULL) {
        LOG_ERROR("param error, null transmit or initKey");
        return INVALID_PARA_NULL_PTR;
    }
    if (initKeyLength == 0 || initKeyLength > MAX_INIT_KEY_LEN) {
        LOG_ERROR("param error, initKeyLength is error = %u", initKeyLength);
        return INVALID_PARA_ERR_SIZE;
    }

    SeCommonSetBindingKeyInput input;
    (void)memset_s(&input, sizeof(SeCommonSetBindingKeyInput), 0, sizeof(SeCommonSetBindingKeyInput));
    if (memcpy_s(input.initKey, MAX_INIT_KEY_LEN, initKey, initKeyLength) != EOK) {
        LOG_ERROR("memcpy_s error");
        return MEM_COPY_ERR;
    }
    input.initKeyLength = initKeyLength;
    input.sid = sid;
    input.initKvn = initKvn;

    uint32_t cmd = SERVICE_WITH_CMD_ID(0, CMD_SET_BINDING_KEY);
    ResultCode ret = transmit(cmd, (const uint8_t *)&input, sizeof(SeCommonSetBindingKeyInput), NULL, NULL);
    LOG_INFO("ret is %u, sid is %u", ret, sid);
    return ret;
}

ResultCode SeCommonDeleteInitKeyProxy(IpcTransmit *transmit, uint8_t *initKey, uint32_t initKeyLength, uint32_t initKvn)
{
    if (transmit == NULL) {
        LOG_ERROR("param error, null transmit or initKey");
        return INVALID_PARA_NULL_PTR;
    }
    (void)initKey;
    (void)initKeyLength;

    uint32_t cmd = SERVICE_WITH_CMD_ID(0, CMD_DELETE_INIT_KEY);

    ResultCode ret = transmit(cmd, (const uint8_t *)&initKvn, sizeof(initKvn), NULL, NULL);
    LOG_INFO("ret is %u", ret);
    return ret;
}