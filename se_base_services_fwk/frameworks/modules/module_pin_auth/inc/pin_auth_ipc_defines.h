/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CODE_MODULES_INC_PIN_AUTH_IPC_DEFINES_H
#define CODE_MODULES_INC_PIN_AUTH_IPC_DEFINES_H

#include <stdint.h>

#include "se_base_services_defines.h"
#include "se_module_pin_auth_defines.h"

#ifdef __cplusplus
extern "C" {
#endif

enum {
    CMD_PIN_AUTH_GET_SLOT_NUM = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 1),
    CMD_PIN_AUTH_ENROLL_LOCAL = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 2),
    CMD_PIN_AUTH_AUTHENTICATION_LOCAL = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 3),
    CMD_PIN_AUTH_ENROLL_REMOTE = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 4),
    CMD_PIN_AUTH_AUTHENTICATION_REMOTE_PREPARE = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 5),
    CMD_PIN_AUTH_AUTHENTICATION_REMOTE = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 6),
    CMD_PIN_AUTH_AUTHENTICATION_REMOTE_ABORT = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 7),
    CMD_PIN_AUTH_CONFIG_SELF_DESTRUCT = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 8),
    CMD_PIN_AUTH_GET_SLOT_STATUS = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 9),
    CMD_PIN_AUTH_CONFIG_FREEZE_POLICY = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 10),
    CMD_PIN_AUTH_GET_FREEZE_POLICY = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 11),
    CMD_PIN_AUTH_ERASE_SINGLE_SLOT = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 12),
    CMD_PIN_AUTH_ERASE_ALL_SLOTS = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, 13),
    CMD_PIN_AUTH_IS_SERVICE_AVALIABLE = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, CMD_IS_SERVICE_AVALIABLE),
    CMD_PIN_AUTH_SET_SERVICE_CONFIG = SERVICE_WITH_CMD_ID(SERVICE_ID_PIN_AUTH, CMD_SET_SERVICE_CONFIG),
};

typedef struct PinEnrollLocalInput {
    uint32_t slotId;
    PinDataHash hash;
} PinEnrollLocalInput;

typedef struct PinEnrollRemoteInput {
    uint32_t slotId;
    PinDataRemoteBase base;
} PinEnrollRemoteInput;

typedef struct PinAuthLocalInput {
    uint32_t slotId;
    PinDataHash hash;
} PinAuthLocalInput;

typedef struct PinAuthRemotePrepareInput {
    uint32_t slotId;
    uint64_t session;
} PinAuthRemotePrepareInput;

typedef struct PinAuthRemoteInput {
    uint32_t slotId;
    uint64_t session;
    PinDataRemoteClientProof proof;
} PinAuthRemoteInput;

typedef struct PinAuthRemoteAbortInput {
    uint32_t slotId;
    uint64_t session;
} PinAuthRemoteAbortInput;

typedef struct SetSelfDestructEnableInput {
    uint32_t slotId;
    uint8_t enable;
    uint16_t destructMaxCnt;
    PinDataHash hash;
} SetSelfDestructEnableInput;

typedef struct SetFreezePolicyInput {
    uint16_t punishStartCnt;
    uint16_t destructMaxCnt;
    uint32_t enableDestructDefault;
} SetFreezePolicyInput;

typedef SetFreezePolicyInput GetFreezePolicyOutput;

#ifdef __cplusplus
}
#endif

#endif // CODE_MODULES_INC_STORAGE_IPC_DEFINES_H