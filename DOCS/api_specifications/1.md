# 前言


## 范围

本规范对需要接入OpenHarmony安全芯片管理框架的北向系统安全业务提供标准API，主要包括：口令认证服务、安全存储业务、高安密码服务、防回退服务。本规范可使上层业务无需感知安全芯片差异、主芯片平台差异等，通过标准API访问安全芯片中的资源和服务，同时也可充分基于安全芯片开展业务创新，从而推动OpenHarmony安全芯片生态发展。


## 术语解释

**表1** 术语定义

| 术语 | 缩略语 | 中文解释 | 
| -------- | -------- | -------- |
| Secure Element | SE | 安全元件 | 
| OpenHarmony Secure Element | OpenHarmony SE/OHSE | OpenHarmony安全芯片的统称 | 
| Application Programming Interface | API | 应用编程接口 | 
| Personal identification number | PIN | 用户口令 | 

**表2** 服务名称定义

| 服务名称定义 | 缩略语 | 中文解释 | 
| -------- | -------- | -------- |
| PIN Authentication | PINAuth | 口令认证服务 | 
| SecureStorage | SecStorage | 安全存储服务 | 
| Secure Element Crypto | SECrypto | 高安密码服务 | 
| Anti-rollback Protection | ARP | 防回滚服务 | 
