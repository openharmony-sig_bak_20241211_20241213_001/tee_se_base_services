/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include "client_common.h"

namespace OHOS {
namespace SeBaseServices {
namespace UnitTest {
using namespace testing;

TEST(ClientCommonTest, SeCommonIsServiceAvailable)
{
    SeServiceStatus status;
    ResultCode ret = SeCommonIsServiceAvailable(SERVICE_ID_PIN_AUTH, &status);
    EXPECT_EQ(ret, SUCCESS);
}

} // namespace UnitTest
} // namespace SeBaseServices
} // namespace OHOS