/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "tee_sharemem_ops_mock.h"

#include <malloc.h>

void *tee_alloc_sharemem_aux(const struct tee_uuid *uuid, uint32_t size)
{
    if (auto *invoker = OHOS::SeBaseServices::UnitTest::TeeAllocSharememAux::GetInstance(); invoker) {
        return invoker->tee_alloc_sharemem_aux(uuid, size);
    }

    return malloc(size);
}

uint32_t tee_free_sharemem(void *addr, uint32_t size)
{
    (void)size;
    free(addr);
    return 0;
}
