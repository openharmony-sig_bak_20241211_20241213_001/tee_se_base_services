/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_LOG_H
#define UNIT_TEST_INC_TEE_LOG_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#define TRACE_S "[Trace]"
#define WARNING_S "[Warning]"
#define ERROR_S "[Error]"

#define SLogTrace(fmt, args...) SLog("%s: " fmt "\n", TRACE_S, ##args)

#define SLogWarning(fmt, args...) SLog("%s: " fmt "\n", ERROR_S, ##args)

#define SLogError(fmt, args...) SLog("%s: " fmt "\n", ERROR_S, ##args)

void SLog(const char *fmt, ...);

#endif // UNIT_TEST_INC_TEE_LOG_H