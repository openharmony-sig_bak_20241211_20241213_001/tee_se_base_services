/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UNIT_TEST_INC_TEE_DYNAMIC_SRV_H
#define UNIT_TEST_INC_TEE_DYNAMIC_SRV_H

#ifndef ENABLE_TESTING
#error only in test mode
#endif

#include <stdio.h>

#include "tee_service_public.h"

struct srv_thread_init_info {
    uint32_t padding;
};

typedef void (*srv_dispatch_fn_t)(tee_service_ipc_msg *msg, uint32_t sndr, tee_service_ipc_msg_rsp *rsp);

struct srv_dispatch_t {
    uint32_t cmd;
    srv_dispatch_fn_t fn;
};

#ifdef __cplusplus
extern "C" {
#endif
TEE_Result tee_srv_get_uuid_by_sender(uint32_t sender, TEE_UUID *uuid);

void tee_srv_unmap_from_task(uint32_t vaAddr, uint32_t size);

int tee_srv_map_from_task(uint32_t inTaskId, uint32_t vaAddr, uint32_t size, uint32_t *virtAddr);

void tee_srv_cs_server_loop(const char *taskName, const struct srv_dispatch_t *dispatch, uint32_t dispatchLen,
    struct srv_thread_init_info *curThread);

#ifdef __cplusplus
}
#endif

#endif // UNIT_TEST_INC_TEE_DYNAMIC_SRV_H